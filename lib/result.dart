import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int finalScore;
  final Function resetFunction;

  String get resultString {
    if (finalScore > 25) {
      return "You are as good as Elihu";
    } else if (finalScore > 15) {
      return "You tried...and that's what matters";
    } else {
      return "You fat!";
    }
  }

  Result(this.finalScore, this.resetFunction);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultString,
            style: TextStyle(
              color: Colors.blue,
              fontSize: 35,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text("Restart Quiz"),
            onPressed: resetFunction,
            textColor: Colors.red,
          )
        ],
      ),
    );
  }
}
