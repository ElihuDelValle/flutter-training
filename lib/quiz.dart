import 'package:flutter/material.dart';
import 'answer.dart';
import 'question.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int index;
  final Function answerCallback;

  Quiz({
    @required this.questions,
    @required this.index,
    @required this.answerCallback,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Question(questions[index]['question']),
          ...(questions[index]['answers'] as List<Map<String, Object>>)
              .map((answer) => Answer(
                    answerText: answer['text'],
                    onPressCallBack: () => answerCallback(answer['score']),
                  ))
              .toList(),
        ],
      ),
    );
  }
}
