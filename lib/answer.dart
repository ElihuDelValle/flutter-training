import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function onPressCallBack;
  final String answerText;

  Answer({this.onPressCallBack, this.answerText});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text(answerText),
        color: Colors.lightGreen,
        onPressed: onPressCallBack,
      ),
    );
  }
}
