import 'package:first_app/result.dart';
import 'package:flutter/material.dart';
import './result.dart';
import './quiz.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionsIndex = 0;
  var _totalScore = 0;

  final questions = const [
    {
      'question': "What type of bike do you use?",
      'answers': [
        {"text": "Road Bike", "score": 7},
        {"text": "TT Bike", "score": 10},
        {"text": "Mountain Bike", "score": 3}
      ]
    },
    {
      'question': "What's your FTP?",
      'answers': [
        {"text": "Below 150", "score": 2},
        {"text": "Between 150 & 200", "score": 5},
        {"text": "Above 200", "score": 10}
      ]
    },
    {
      'question': "What's your weight?",
      'answers': [
        {"text": "Below 50kg", "score": 10},
        {"text": "Between 50kg and 60 kg", "score": 8},
        {"text": "Above 60kg", "score": 6},
        {"text": "I\'m fat", "score": -10},
      ]
    }
  ];

  void _answerQuestion(int score) {
    _totalScore += score;
    print("Index: " + _questionsIndex.toString());
    print("Length: " + questions.length.toString());
    setState(() {
      _questionsIndex = _questionsIndex + 1;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionsIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Elihu App'),
        ),
        body: _questionsIndex < questions.length
            ? Quiz(
                questions: questions,
                index: _questionsIndex,
                answerCallback: _answerQuestion,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
